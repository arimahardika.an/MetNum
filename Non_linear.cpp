/*
* Copyright 2016 Ari Mahardika
* Email : arimahardika.an@gmail.com
* Git   : https://gitlab.com/u/Ari.Mahardika
*/
#include <iostream>
#include <iomanip>

using namespace std;

int main(){
	float start, end;
	float interval, pembagi;
	float plus_negative;
	int perulangan;
	float x=0,y=0;
	float x_baru, y_baru;
	
	//rumus : x^2+x-6
	
	cout<<"Persamaan Non-Linear"<<endl;
	cout<<"==========*==========*=========="<<endl;
	cout<<"Rumus yang digunakan   : x^2+x-6"<<endl<<endl;
	cout<<"Masukkan Nilai awal    : ";
	cin>>start;
	cout<<"Masukkan Nilai akhir   : ";
	cin>>end;
	cout<<"Dipotong Berapa Kali   : ";
	cin>>pembagi;
	interval = (end-start)/pembagi;
	cout<<"Interval               : "<<interval<<endl;
	cout<<"Berapa Kali Perulangan : ";
	cin>>perulangan;
	plus_negative = start*end;
	cout<<"Perkiraan Hasil        : ";
	if(plus_negative > 0){
		cout<<"Tidak berpotongan atau berpotongan di angka genap"<<endl;
	}
	if(plus_negative < 0){
		cout<<"Memiliki perpotongan minimal 1, dan berpotongan di angka ganjil"<<endl;
	}
	if(plus_negative == 0){
		cout<<"0"<<endl;
	}
	cout<<endl<<"==========*==========*=========="<<endl;
	
	for(int i = 0;i<=perulangan;i++){
		if(i==0){
			x=start+interval;
			y=x*x+x-6;
			cout<<i<<setw(10)<<x<<setw(10)<<y<<endl;
			x_baru = x;
			y_baru = y;
		}
		else{
			x=x_baru+interval;
			y=x*x+x-6;
			cout<<i<<setw(10)<<x<<setw(10)<<y<<endl;
			if((y*y_baru)<0){
				break;
			}
			x_baru=x;
			y_baru=y;
			
		}
		
	}
	
return 0;
}

