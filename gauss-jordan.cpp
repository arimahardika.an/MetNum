#include <iostream>
#include <iomanip>

using namespace std;

int main(){
	int n;
	float A[100][101], c, sum=0.0, x[100], gjc;
	
	//input array ----------------------------	
	cout<<"===== Input ====="<<endl;
	cout<<"Masukan ukuran matrix = ";
	cin>>n;
	cout<<"Masukkan angka matrix = "<<endl;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n+1;j++){
			cout<<"A[" << i << "][" << j <<"] : ";
			cin>>A[i][j];
		}
	}

	//cetak ----------------------------------
	cout<<endl<<"===== Cetak ====="<<endl<<endl;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n+1;j++){
			cout<<setw(5)<<A[i][j]<<" ";			
		}
		cout<<endl;
	}
	cout<<endl;
	
	//pemrosesan array ---------------------------
	cout<<endl<<"Proses Pertama"<<endl;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++){
			if(j!=i){	
				c=A[j][i]/A[i][i];
				for(int k=1;k<=n+1;k++){
					A[j][k]=A[j][k]-c*A[i][k];
				}
			}
		}
		//cetak ----------------------------------
		for(int i=1;i<=n;i++){
			for(int j=1;j<=n+1;j++){
				cout<<setw(10)<<A[i][j]<<" ";			
			}
			cout<<endl;
		}
		//cetak ----------------------------------
	cout<<endl;
	}
	/*pemrosesan array ---------------------------
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++){
			if(A[i][i]!=1){
				gjc = 1/A[i][i];
			}
		}
	}*/
	
	//cetak setelah proses -------------------
	cout<<endl<<"===== Array Setelah Proses ====="<<endl<<endl;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n+1;j++){
			cout<<setw(10)<<A[i][j]<<" ";			
		}
		cout<<endl;
	}	
	
	for(int i=n;i>=1;i--){
		for(int j=n+1;j>=1;j--)
	}
	
	//backward subtitution -------------------
		//mengisi hasil[n]
	x[n] = A[n][n+1]/A[n][n];
	
	for(int i=n-1;i>=1;i--){
		sum=0;
		for(int j=i+1;j<=n;j++){
			sum=sum+A[i][j]*x[j];
		}
		x[i]=(A[i][n+1]-sum)/A[i][i];
	}
	//backward subtitution -------------------
	
	//hasil akhir ----------------------------
	cout<<endl<<"===== Hasil Akhir ===="<<endl<<endl;
	for(int i=1;i<=n;i++){
		cout<<"x"<<i<<": "<<x[i]<<endl;
	}
	//hasil akhir ----------------------------
	
return 0;
}

