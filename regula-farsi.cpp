/*
* Copyright 2016 Ari Mahardika
* Email : arimahardika.an@gmail.com
* Git   : https://gitlab.com/u/Ari.Mahardika
*/

#include <iostream>
#include <iomanip>

using namespace std;

int main(){
	
	double a,b,t;
	double fa,fb,ft;
	double anew, bnew,tnew;
	int perulangan;
	float error = 0.01;
	static int prec = 5;
	static int sw = 20;
	
	cout<<"Bisection\n";
	cout<<"Persamaan yang digunakan : x^2-5x+6\n";
	cout<<"Error                    : 0,00001\n";
	cout<<"=====================================\n";
	//menentukan nilai a, b, dan t
	cout<<"Masukkan nilai a : ";
	cin>>a;
	cout<<"Masukkan nilai b : ";
	cin>>b;
	//menentukan banyaknya perulangan
	cout<<"Loop berapa kali : ";
	cin>>perulangan;
	cout<<"=====================================\n";
	
	//proses
	cout<<setw(5)<<"i"<<setw(sw)<<"a"<<setw(sw)<<"f(a)"<<setw(sw)<<"t"<<setw(sw)<<"f(t)"<<setw(sw)<<"b"<<setw(sw)<<"f(b)"<<endl;
	for(int i=1;i<=perulangan;i++){
		if(i==1){
			
			fa=a*a-5*a+6; 	//mendapatkan nilai f(a)
			fb=b*b-5*b+6; 	//mendapatkan nilai f(b)
			
			t=((fb*a)-(fa*b))/(fb-fa);	//mendapatkan nilai t
			
			ft=t*t-5*t+6;	//mendapatkan nilai f(t)
			
			cout<<setw(5)<<i;
			
			cout<<fixed<<setprecision(prec)<<setw(sw)<<a;
			cout<<fixed<<setprecision(prec)<<setw(sw)<<fa;
			
			cout<<fixed<<setprecision(prec)<<setw(sw)<<t;
			cout<<fixed<<setprecision(prec)<<setw(sw)<<ft;
			
			cout<<fixed<<setprecision(prec)<<setw(sw)<<b;
			cout<<fixed<<setprecision(prec)<<setw(sw)<<fb<<endl;
		}
		else{
			
			/*
			* mendapatkan nilai anew, jika fa*fb>0,
			* ambil nilai t, jika tidak ambil nilai a
			*/
			
			if((fa*ft)>0){	
				anew = t;
			}
			else{
				anew = a;
			}
			
			if((fb*ft)>0){
				bnew = t;
			}
			else{
				bnew = b;
			}
			
			a=anew; //menggunakan nilai anew
			b=bnew; //menggunakan nilai bnew
			
			fa=a*a-5*a+6; 	//mendapatkan nilai f(a)
			fb=b*b-5*b+6; 	//mendapatkan nilai f(b)
			t=((fb*a)-(fa*b))/(fb-fa);		//mendapatkan nilai t
			ft=t*t-5*t+6;	//mendapatkan nilai f(t)
			if((fa*fb)>0){
				break;
			}
			cout<<setw(5)<<i;
			
			cout<<fixed<<setprecision(prec)<<setw(sw)<<a;
			cout<<fixed<<setprecision(prec)<<setw(sw)<<fa;
			
			cout<<fixed<<setprecision(prec)<<setw(sw)<<t;
			cout<<fixed<<setprecision(prec)<<setw(sw)<<ft;
			
			cout<<fixed<<setprecision(prec)<<setw(sw)<<b;
			cout<<fixed<<setprecision(prec)<<setw(sw)<<fb<<endl;
		}
		/*
		if((b-a)/2<error){
			cout<<"Hasil ditemukan di perulangan ke "<<i<<endl;
			cout<<"=====================================\n";
			break;
		}
		*/
	}
return 0;
}
